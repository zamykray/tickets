Tickets parser
=========

# Installation
Required PHP >= 5.6.4

### Clone repository ###
```bash
git clone https://bitbucket.org/zamykray/tickets.git
```

### Install composer packages
```bash
composer install
```

### Create .env from example and edit it with your configs
```bash
cp .env.example .env
```

### Run Phing init build target to build configs
```bash
vendor/bin/phing init
```

# Apache config example
```
<VirtualHost *:80>
    DocumentRoot "/path/to/document/root/tickets/public"
    ServerName tickets
    ErrorLog "logs/localhost-error.log"
    CustomLog "logs/localhost-access.log" common
    DirectoryIndex index.php

    <Directory "/path/to/document/root/tickets/public">
        AddDefaultCharset utf-8
        Options FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory> 
</VirtualHost>
```