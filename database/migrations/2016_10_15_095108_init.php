<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Init extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('station', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('code');
            $table->unique('code');

            $table->string('description');

            $table->timestamps();
        });
        Schema::create('train_type', function (Blueprint $table) {
            $table->increments('id');

            $table->string('code');
            $table->unique('code');

            $table->string('description');

            $table->timestamps();
        });
        Schema::create('train', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('number');
            $table->unique('number');

            $table->integer('type_id')->unsigned();
            $table->foreign('type_id')
                ->references('id')->on('train_type');

            $table->time('departure_at');
            $table->time('arrival_at');

            $table->integer('departure_station_id')->unsigned();
            $table->foreign('departure_station_id')
                ->references('id')->on('station');

            $table->integer('arrival_station_id')->unsigned();
            $table->foreign('arrival_station_id')
                ->references('id')->on('station');

            $table->timestamps();
        });

        Schema::create('ticket_class', function (Blueprint $table) {
            $table->increments('id');

            $table->string('code');
            $table->unique('code');

            $table->string('description');

            $table->timestamps();
        });
        Schema::create('ticket_tariff', function (Blueprint $table) {
            $table->increments('id');

            $table->string('code');
            $table->unique('code');

            $table->string('description');

            $table->timestamps();
        });
        Schema::create('ticket_tariff_type', function (Blueprint $table) {
            $table->increments('id');

            $table->string('code');
            $table->unique('code');

            $table->string('description');

            $table->timestamps();
        });
        Schema::create('ticket', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('train_id')->unsigned();
            $table->foreign('train_id')
                ->references('id')->on('train');

            $table->integer('class_id')->unsigned();
            $table->foreign('class_id')
                ->references('id')->on('ticket_class');

            $table->integer('tariff_id')->unsigned();
            $table->foreign('tariff_id')
                ->references('id')->on('ticket_tariff');

            $table->integer('tariff_type_id')->unsigned()->nullable();
            $table->foreign('tariff_type_id')
                ->references('id')->on('ticket_tariff_type');

            $table->unique(['train_id', 'class_id', 'tariff_id', 'tariff_type_id']);

            $table->integer('cnt');
            $table->float('price');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
