@extends('master')

@section('title', 'Page Title')

@section('content')
    <table border="1" cellspacing="0" cellpadding="5">
        <thead>
            <tr>
                <th>Train Number</th>
                <th>Train Code</th>
                <th>Train Description</th>
                <th>Departure Time</th>
                <th>Arrival Time</th>
                <th>Travel time</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $train['number'] }}</td>
                <td>{{ $train['code'] }}</td>
                <td>{{ $train['description'] }}</td>
                <td>{{ $train['departure_at'] }}</td>
                <td>{{ $train['arrival_at'] }}</td>
                <td>{{ $train['travel_time'] }}</td>
            </tr>
        </tbody>
    </table>

    <div style="margin-top:20px;">
    <table border="1" cellspacing="0" cellpadding="5">
        <thead>
            <tr>
                <th>Class Description</th>
                <th>Tariff Description</th>
                <th>Availability</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($tickets as $ticket)
            <tr>
                <td>{{ $ticket['class_description'] }}</td>
                <td>{{ $ticket['tariff_description'] }}</td>
                <td>{{ $ticket['cnt'] }}</td>
                <td align="right">{{ $ticket['price'] }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
@stop