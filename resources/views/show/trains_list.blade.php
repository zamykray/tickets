@extends('master')

@section('title', 'Page Title')

@section('content')
    <table border="1" cellspacing="0" cellpadding="5">
        <thead>
            <tr>
                <th>Train Number</th>
                <th>Train Code</th>
                <th>Train Description</th>
                <th>Departure Time</th>
                <th>Arrival Time</th>
                <th>Travel time</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($trains as $train)
            <tr>
                <td>{{ $train['number'] }}</td>
                <td>{{ $train['code'] }}</td>
                <td>{{ $train['description'] }}</td>
                <td>{{ $train['departure_at'] }}</td>
                <td>{{ $train['arrival_at'] }}</td>
                <td>{{ $train['travel_time'] }}</td>
                <td><a href="/trains/show/{{ $train['id'] }}">view</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop