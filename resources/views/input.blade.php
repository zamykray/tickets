@extends('master')

@section('title', 'Page Title')

@section('content')
    <form method="post" action="/trains/input" enctype="multipart/form-data">
        <input type="file" name="xml[]" multiple="multiple" />
        <button type="submit">Import</button>
    </form>
    @if ($result)
    <div>
        @foreach ($result as $fileResult)
            <h3>{{ $fileResult['file'] }}</h3>
            Uploaded {{ count($fileResult['importedTrains']) }} trains from {{ $fileResult['totalCount'] }}
            @if ($fileResult['errors'])
            <p>Errors:</p>
            <ul>
                @foreach ($fileResult['errors'] as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif
        @endforeach
    </div>
    @endif
@stop