<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->addRoute(['GET','POST'], '/trains/input', ['uses' => 'TrainsController@input']);
$app->get('/trains/show', ['uses' => 'TrainsController@showAll']);
$app->get('/trains/show/{trainId}', ['uses' => 'TrainsController@show']);

