<?php

namespace App\Http\Controllers;

use App\Model\Ticket\Ticket;
use App\Model\Train;
use App\Service\XmlLoader;
use App\Service\Import\TrainImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class TrainsController extends Controller
{
    public function input(Request $request)
    {
        $result = [];
        if ($request->isMethod('POST')) {
            $files = $request->files->all();
	        if (!empty($files['xml'])) {
		        /** @var UploadedFile $file */
		        foreach ( $files['xml'] as $file ) {
			        $content              = file_get_contents( $file->getRealPath() );
			        $trainNodes           = XmlLoader::getXmlTags( $content, 'trainInformation' );
			        $importResult         = TrainImport::import( $trainNodes );
			        $importResult['file'] = $file->getClientOriginalName();

			        $result[] = $importResult;
		        }
	        }
        }

        return view('input', ['result' => $result]);
    }

    public function showAll()
    {
        $trains = Train
            ::join('train_type', 'train.type_id', 'train_type.id')
            ->select('train.*', 'train_type.code', 'train_type.description')
            ->orderBy('departure_at')
            ->get();

        return view('show.trains_list', ['trains' => $trains]);
    }

    public function show($trainId)
    {
        $train = Train
            ::join('train_type', 'train.type_id', 'train_type.id')
            ->select('train.*', 'train_type.code', 'train_type.description')
            ->where('train.id', $trainId)
            ->firstOrFail();

        DB::statement("set session sql_mode = ''");
        $tickets = Ticket
            ::join('ticket_class', 'ticket.class_id', 'ticket_class.id')
            ->join('ticket_tariff', 'ticket.tariff_id', 'ticket_tariff.id')
            ->selectRaw('SUM(ticket.cnt) as cnt, ticket.price, ticket_class.description as class_description, ticket_tariff.description as tariff_description')
            ->where('ticket.train_id', $trainId)
            ->groupBy('ticket.class_id', 'ticket.tariff_id')
            ->orderBy('class_description')
            ->orderBy('tariff_description')
            ->get();

        return view('show.train', [
            'train' => $train,
            'tickets' => $tickets,
        ]);
    }
}