<?php

namespace App\Service\Import\Ticket;

use App\Model\Ticket\TariffType;
use App\Model\Ticket\TicketClass;
use App\Model\Train;
use App\Service\Import\AbstractImport;

class TariffTypeImport extends AbstractImport
{
    const FIELDS = [
        'tariffCode' => [
            'type' => 'string',
            'required' => false,
        ],
        'tariffDescription' => [
            'type' => 'string',
            'required' => false,
        ],
        'tariff' => 'array',
    ];

    public static function import(\SimpleXMLElement $tariffTypeNode, Train $train, TicketClass $ticketClass)
    {
        self::validate($tariffTypeNode);

        $tariffType = null;
        if ((string)$tariffTypeNode->tariffCode !== '') {
            $tariffType = TariffType::where('code', $tariffTypeNode->tariffCode)->first();
            if (!$tariffType) {
                $tariffType = new TariffType();
                $tariffType->code = $tariffTypeNode->tariffCode;
                $tariffType->description = $tariffTypeNode->tariffDescription;
                $tariffType->save();
            }
        }

        foreach ($tariffTypeNode->tariff as $ticketNode) {
            TicketImport::import($ticketNode, $train, $ticketClass, $tariffType);
        }
    }
}