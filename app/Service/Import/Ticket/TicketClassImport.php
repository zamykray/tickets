<?php

namespace App\Service\Import\Ticket;

use App\Model\Ticket\TicketClass;
use App\Model\Train;
use App\Service\Import\AbstractImport;

class TicketClassImport extends AbstractImport
{
    const FIELDS = [
        'classCode' => 'string',
        'classDescription' => 'string',
        'tariffs' => 'array',
    ];

    /**
     * @param Train $train
     * @param \SimpleXMLElement $classNode
     */
    public static function import(\SimpleXMLElement $classNode, Train $train)
    {
        self::validate($classNode);

        $ticketClass = TicketClass::where('code', $classNode->classCode)->first();
        if (!$ticketClass) {
            $ticketClass = new TicketClass();
            $ticketClass->code = $classNode->classCode;
            $ticketClass->description = $classNode->classDescription;
            $ticketClass->save();
        }

        foreach ($classNode->tariffs as $tariffTypeNode) {
            TariffTypeImport::import($tariffTypeNode, $train, $ticketClass);
        }
    }
}