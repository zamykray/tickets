<?php

namespace App\Service\Import\Ticket;

use App\Model\Ticket\Tariff;
use App\Model\Ticket\TariffType;
use App\Model\Ticket\Ticket;
use App\Model\Ticket\TicketClass;
use App\Model\Train;
use App\Service\Import\AbstractImport;

class TicketImport extends AbstractImport
{
    const FIELDS = [
        'tariffCode'    => 'string',
        'tariffDecription' => 'string',
        'availability'  => 'int',
        'price'         => 'int',
    ];

    /**
     * @param \SimpleXMLElement $tariffNode
     * @param Train $train
     * @param TicketClass $ticketClass
     * @param TariffType $tariffType
     */
    public static function import(\SimpleXMLElement $tariffNode, Train $train, TicketClass $ticketClass, TariffType $tariffType = null)
    {
        self::validate($tariffNode);

        $tariffCode = self::parseTariffCode($tariffNode->tariffCode);
        $tariff = Tariff::where('code', $tariffCode)->first();
        if (!$tariff) {
            $tariff = new Tariff();
            $tariff->code = $tariffCode;
            $tariff->description = $tariffNode->tariffDecription;
            $tariff->save();
        }

        $tariffTypeId = $tariffType ? $tariffType->id : null;
        $ticket = Ticket
            ::where('train_id', $train->id)
            ->where('class_id', $ticketClass->id)
            ->where('tariff_id', $tariff->id)
            ->where('tariff_type_id', $tariffTypeId)
            ->first();
        if (!$ticket) {
            $ticket = new Ticket();
            $ticket->train_id = $train->id;
            $ticket->class_id = $ticketClass->id;
            $ticket->tariff_id = $tariff->id;
            $ticket->tariff_type_id = $tariffTypeId;
        }
        $ticket->cnt = (int)$tariffNode->availability;
        $ticket->price = self::parsePrice($tariffNode->price);

        $ticket->save();
    }

    private static function parseTariffCode($code)
    {
        $codeInfo = explode('|', $code);

        return $codeInfo[0];
    }
}