<?php

namespace App\Service\Import;

class Validator
{
    /**
     * @param \SimpleXMLElement $time
     * @return bool
     */
    public static function isTime(\SimpleXMLElement $time)
    {
        return preg_match("/(:?2[0-3]|[01][0-9])(:?[0-5][0-9])/", (string)$time) > 0;
    }

    /**
     * @param \SimpleXMLElement $val
     * @return bool
     */
    public static function isArray(\SimpleXMLElement $val)
    {
        return $val->children()->count() > 0;
    }

    /**
     * @param \SimpleXMLElement $val
     * @return bool
     */
    public static function isInt(\SimpleXMLElement $val)
    {
        return (int)$val > 0;
    }

    /**
     * @param \SimpleXMLElement $val
     * @return bool
     */
    public static function isInteger(\SimpleXMLElement $val)
    {
        return self::isInt($val);
    }

    /**
     * @param \SimpleXMLElement $val
     * @return bool
     */
    public static function isString(\SimpleXMLElement $val)
    {
        return true;
    }
}