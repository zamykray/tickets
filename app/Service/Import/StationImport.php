<?php

namespace App\Service\Import;

use App\Model\Station;

class StationImport extends AbstractImport
{
    const FIELDS = [
        'stationCode' => 'int',
        'stationDescription' => 'string',
    ];

    public static function import(\SimpleXMLElement $stationNode)
    {
        self::validate($stationNode);

        $stationCode = (int)$stationNode->stationCode;
        $station = Station::where('code', $stationCode)->first();
        if (!$station) {
            $station = new Station();
            $station->code = $stationCode;
            $station->description = $stationNode->stationDescription;
            $station->save();
        }

        return $station->id;
    }
}