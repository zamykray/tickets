<?php

namespace App\Service\Import;

use App\Model\TrainType;

class TrainTypeImport extends AbstractImport
{
    const FIELDS = [
        'trainCode' => 'string',
        'trainDescription' => [
            'type' => 'string',
            'required' => false,
        ],
    ];

    public static function import(\SimpleXMLElement $trainTypeNode)
    {
        self::validate($trainTypeNode);

        $trainTypeCode = self::parseCode($trainTypeNode->trainCode);
        $trainType = TrainType::where('code', $trainTypeCode)->first();
        if (!$trainType) {
            $trainType = new TrainType();
            $trainType->code = $trainTypeCode;
            $trainType->description = $trainTypeNode->trainDescription;
            $trainType->save();
        }

        return $trainType->id;
    }

    /**
     * @param \SimpleXMLElement[]|\SimpleXMLElement $code
     * @return string
     */
    private static function parseCode(\SimpleXMLElement $code)
    {
        if (is_numeric((string)$code)) {
            return (string)(int)$code;
        }

        return (string)$code;
    }
}