<?php

namespace App\Service\Import;

use App\Exceptions\XmlException;

abstract class AbstractImport
{
    /**
     * Array. Used for validation of fields. Format:
     *  ["xml_column_name_variant1|xml_column_name_variant2" => field_type|field_options_array]
     *  field_type @see App\Service\Import\Validator
     *  field_options_array keys: (boolean) required (default: true), (string) type
     */
    const FIELDS = [];

    /**
     * @param \SimpleXMLElement[]|\SimpleXMLElement $nodes
     * @throws XmlException
     */
    protected static function validate($nodes)
    {
        if (!is_array($nodes)) {
            $nodes = [$nodes];
        }
        foreach ($nodes as $node) {
            $invalidFields = [];

            foreach (static::FIELDS as $column => $options) {
                if (!is_array($options)) {
                    $options = [
                        'required' => true,
                        'type' => $options,
                    ];
                }
                $columns = explode('|', $column);
                $firstColumn = $columns[0];

                $exists = array_values(array_filter($columns, function ($val) use ($node) {
                    return property_exists($node, $val) && ((string)$node->$val || (string)$node->val === '0');
                }));
                if (!$exists) {
                    if ($options['required']) {
                        $invalidFields[$column] = '';
                    } else {
                        $node->$firstColumn = '';
                    }
                } else {
                    $columnName = $exists[0];
                    $validType = call_user_func(array(Validator::class, 'is' . ucfirst($options['type'])), $node->$columnName);
                    if ($validType) {
                        // Set access to property by first column variant
                        if (!isset($node->$firstColumn)) {
                            foreach ($node->$columnName->children() as $childKey => $childNode) {
                                $node->$firstColumn->$childKey = $childNode;
                            }
                        }
                    } else {
                        $invalidFields[$column] = $node->$columnName;
                    }
                }
            }
            if ($invalidFields) {
                $fieldsErrorStr = [];
                foreach ($invalidFields as $fieldKey => $fieldVal) {
                    $fieldsErrorStr[] = sprintf('%s = "%s"', $fieldKey, $fieldVal);
                }
                throw new XmlException(sprintf("Invalid fields: %s. In node %s", implode(', ', $fieldsErrorStr), $node->getName()));
            }
        }
    }

    /**
     * @param \SimpleXMLElement|\SimpleXMLElement[] $time
     * @return string
     */
    protected static function parseTime($time)
    {
        return substr_replace((string)$time, ':', 2, 0);
    }

    /**
     * @param \SimpleXMLElement|\SimpleXMLElement[] $price
     * @return string
     */
    protected static function parsePrice($price)
    {
        return (float)substr_replace((string)$price, '.', -2, 0);
    }
}