<?php

namespace App\Service\Import;

use App\Exceptions\XmlException;
use App\Model\Train;
use App\Service\Import\Ticket\TicketClassImport;
use Illuminate\Support\Facades\DB;

class TrainImport extends AbstractImport
{
    const FIELDS = [
        'trainNumber'       => 'int',
        'trainType'         => 'array',
        'departureTime'     => 'time',
        'arrivalTime'       => 'time',
        'departureStation'  => 'array',
        'arrivalStation|destino' => 'array',
        'clases'            => 'array',
    ];

    /**
     * @param \SimpleXMLElement[] $trainNodes
     * @return array
     * @throws \Exception
     */
    public static function import(array $trainNodes)
    {
        $importedTrains = new \SplObjectStorage();
        $errors = [];
        foreach ($trainNodes as $trainNode) {
            try {
                DB::beginTransaction();

                self::validate($trainNode);

                $trainNumber = (int)$trainNode->trainNumber;
                $train = Train::where('number', $trainNumber)->first();
                if (!$train) {
                    $train = new Train;
                    $train->number = $trainNumber;
                }
                $train->type_id         = TrainTypeImport::import($trainNode->trainType[0]);
                $train->departure_at    = self::parseTime($trainNode->departureTime);
                $train->arrival_at      = self::parseTime($trainNode->arrivalTime);
                $train->departure_station_id = StationImport::import($trainNode->departureStation[0]);
                $train->arrival_station_id = StationImport::import($trainNode->arrivalStation[0]);
                $train->save();

                foreach ($trainNode->clases as $classNode) {
                    TicketClassImport::import($classNode, $train);
                }

                DB::commit();
                $importedTrains->attach($train);

            } catch (XmlException $e) {
                DB::rollBack();
                $errors[] = $e->getMessage();

            } catch (\Exception $e) {
                DB::rollBack();
                throw $e;
            }
        }

        return [
            'importedTrains' => $importedTrains,
            'errors' => $errors,
            'totalCount' => count($importedTrains) + count($errors),
        ];
    }

}
