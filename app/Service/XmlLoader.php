<?php

namespace App\Service;

use App\Exceptions\XmlException;

class XmlLoader
{
    /**
     * @param string $xmlContent
     * @param string $tagName
     * @return \SimpleXMLElement[]
     * @throws XmlException
     */
    public static function getXmlTags($xmlContent, $tagName)
    {
        $tagNameEscaped = preg_quote($tagName);
        if (preg_match("#<$tagNameEscaped>.+</$tagNameEscaped>#us", $xmlContent, $matches)) {
            $data = "<root>" . $matches[0] . "</root>";
            libxml_use_internal_errors(true);
            $xml = simplexml_load_string($data);
            if ($xml === false) {
                $errors = [];
                foreach (libxml_get_errors() as $error) {
                    $errors[] =  $error->message;
                }
                libxml_clear_errors();
                throw new XmlException(implode('. ', $errors));
            }

            return $xml->xpath('//' . $tagName);
        }

        return [];
    }
}