<?php
namespace App\Model;

use App\Model\Ticket\Ticket;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property int $number
 * @property int $type_id
 * @property string departure_at
 * @property string arrival_at
 * @property int $departure_station_id
 * @property int $arrival_station_id
 */
class Train extends Model
{
    protected $table = 'train';

    public function type()
    {
        return $this->belongsTo(TrainType::class);
    }

    public function departureStation()
    {
        return $this->belongsTo(Station::class);
    }

    public function arrivalStation()
    {
        return $this->belongsTo(Station::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    public function getNumberAttribute($val)
    {
        return sprintf('%05d', $val);
    }

    public function getDepartureAtAttribute($time)
    {
        return substr($time, 0, 5);
    }

    public function getArrivalAtAttribute($time)
    {
        return substr($time, 0, 5);
    }

    public function getTravelTimeAttribute()
    {
        return $this->timeDifference($this->departure_at, $this->arrival_at);
    }


    private function timeDifference($time1, $time2)
    {
        $time1 = strtotime("1980-01-01 $time1");
        $time2 = strtotime("1980-01-01 $time2");

        if ($time2 < $time1) {
            $time2 += 86400;
        }

        return date("G:i", strtotime("1980-01-01 00:00:00") + ($time2 - $time1));
    }
}