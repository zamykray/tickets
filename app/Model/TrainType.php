<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $code
 * @property string $description
 */
class TrainType extends Model
{
    protected $table = 'train_type';

    public function trains()
    {
        return $this->hasMany(Train::class);
    }
}