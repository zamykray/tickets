<?php

namespace App\Model\Ticket;

use App\Model\Train;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $train_id
 * @property integer $tariff_id
 * @property integer $class_id
 * @property integer $tariff_type_id
 * @property integer $cnt
 * @property float $price
 */
class Ticket extends Model
{
    protected $table = 'ticket';

    private $uahToEuro = 25;

    public function train()
    {
        return $this->belongsTo(Train::class);
    }

    public function tariff()
    {
        return $this->belongsTo(Tariff::class);
    }

    public function ticketClass()
    {
        return $this->belongsTo(TicketClass::class);
    }

    public function tariffType()
    {
        return $this->belongsTo(TariffType::class);
    }

    public function getPriceAttribute($val)
    {
        return number_format($val * $this->uahToEuro, 2) . ' UAH';
    }
}