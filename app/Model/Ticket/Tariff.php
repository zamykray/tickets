<?php

namespace App\Model\Ticket;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $code
 * @property string $description
 */
class Tariff extends Model
{
    protected $table = 'ticket_tariff';

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }
}