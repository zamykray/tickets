<?php

namespace App\Model\Ticket;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $code
 * @property string $description
 */
class TicketClass extends Model
{
    protected $table = 'ticket_class';

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}