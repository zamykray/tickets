<?php

namespace App\Model\Ticket;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $code
 * @property string $description
 */
class TariffType extends Model
{
    protected $table = 'ticket_tariff_type';

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}