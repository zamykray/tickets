<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property int $code
 * @property string $description
 */
class Station extends Model
{
    protected $table = 'station';
}